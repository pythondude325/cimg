#!/bin/bash

CC=clang
CFLAGS="-Wall -Wextra -O3"
BINARY=cimg
SOURCES="cimg.c base64_encode.c"

$CC $SOURCES -o $BINARY $CFLAGS
